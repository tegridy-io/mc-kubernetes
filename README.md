# Kubernetes ready Minecraft

Container image for Minecraft.

## Feed the Beast

Build image:
```shell
TAG=<INSERT_TAG>
docker build --target ftb -t registry.gitlab.com/tegridy-io/mc-kubernetes/feedthebeast:${TAG} .
```

## Spigot

Build image:
```shell
TAG=<INSERT_TAG>
docker build --target spigot -t registry.gitlab.com/tegridy-io/mc-kubernetes/spigot:${TAG} .
```
